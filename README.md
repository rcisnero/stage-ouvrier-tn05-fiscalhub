# Stage Ouvrier TN05 FiscalHub

[![BASH](https://img.shields.io/badge/-BASH-success.svg)](https://gitlab.utc.fr/rcisnero/stage-ouvrier-tn05-fiscalhub/)
[![HTML](https://img.shields.io/badge/-HTML-important.svg)](https://gitlab.utc.fr/rcisnero/stage-ouvrier-tn05-fiscalhub/)
[![CSS](https://img.shields.io/badge/-CSS-critical.svg)](https://gitlab.utc.fr/rcisnero/stage-ouvrier-tn05-fiscalhub/)
[![Grafana](https://img.shields.io/badge/-Grafana-informational.svg)](https://gitlab.utc.fr/rcisnero/stage-ouvrier-tn05-fiscalhub/)
[![InfluxDB](https://img.shields.io/badge/-InfluxDB-blueviolet.svg)](https://gitlab.utc.fr/rcisnero/stage-ouvrier-tn05-fiscalhub/)
